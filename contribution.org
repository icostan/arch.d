* Arch
*** installed package statistics
    #+begin_src sh
      pacman -S pgkstats
    #+end_src
*** hardware list
    #+begin_src sh
      pacman -S hw-probe
    #+end_src
*** orphan packages
    #+begin_src sh
      pacman -S aurphan
    #+end_src
*** reproducible builds
    #+begin_src sh
      pacman -S rebuilderd archlinux-repro
      systemctl start rebuilderd.service
      systemctl start rebuilderd-worker@drakarys.service

      # add/edit/change profiles to sync
      # echo source = "https://mirrors.chroot.ro/archlinux/$repo/os/$arch" >> /etc/rebuilderd-sync.conf
      systemctl start rebuilderd-sync@<PROFILE_NAME>.service
    #+end_src
*** package checker
    #+begin_src sh
      pacman -S namcap
    #+end_src
