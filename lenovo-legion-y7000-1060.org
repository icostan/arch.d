* Lenovo Legion Y7000-1060
** Software
    - https://wiki.archlinux.org/index.php/Libinput
** Hardware
*** sound card
*** nvidia GTX 1060 gpu
    - NVIDIA Optimus wiki
    #+begin_src sh
      pacman -S nvtop
    #+end_src
*** wireless
*** touchpad
    #+begin_src sh
      pacman -S xf86-input-libinput
      cat <<EOF > /etc/X11/xorg.conf.d/30-touchpad.conf
      Section "InputClass"
        Identifier "touchpad"
	MatchIsTouchpad "on"
	Driver "libinput"
	Option "Tapping" "on"
      EndSection
      EOF
    #+end_src
*** keyboard
    #+begin_src sh
      cat <<EOF > /etc/X11/xorg.conf.d/00-keyboard.conf
      Section "InputClass"
        Identifier "system-keyboard"
	MatchIsKeyboard "on"
	Option "XkbLayout" "us"
	Option "XkbOptions" "caps:ctrl_modifier"
      EndSection
      EOF
    #+end_src
*** monitor
    #+begin_src sh
      cat <<EOF > /etc/X11/xorg.conf.d/90-monitor.conf
      Section "Monitor"
        Identifier             "eDP-1"
	DisplaySize            344 194
      EndSection
      EOF
    #+end_src
*** camera
** hw-probe
   #+begin_src sh
     pacman -S  hdparm smartmontools efibootmgr memtester mesa-demos acpica # check hw-probe.pl for all tools
     rua install edid-decode-git inxi
     rua install hw-probe
   #+end_src
